package  com.slabs.rcguattendance;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Proxy;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class  MainActivity  extends Activity  {

	public static String  URL , id;
    public static String name, pin, year, branch, section, email, blood, phone, hostel, local, college, dob, gender, term, hometown, bloodwilling, registered;
    public static int imagetotsize, imagedownsize;
    String pincheck="";
	ImageView Iface;
	public static EditText Eid;
    public static TextView Tevent, Ename, Epin, Eyear, Ebranch, Esection, Eemail, Eblood, Ephone, Tpassword, Onlinestatus;
	Button Bgetdetails, Bsubmit;
    public static EditText Eseteventname, Epassword;
    public static TextView Tseteventname, Tnote;
    public static Button Bseteventname, Bpassword;
	StringBuilder text = new StringBuilder();
	String text1;
    public static boolean onlineflag=true, Enablephoto=true, connectionflag;



    public static int count;
			@Override
			public  void  onCreate(Bundle  savedInstanceState)  {
				super.onCreate(savedInstanceState);
				setContentView(R.layout.activity_main);





                Iface	= (ImageView)  findViewById(R.id.iface);
                Tevent  = (TextView) findViewById(R.id.tevent);
                Eid     = (EditText) findViewById(R.id.eid);
                Ename   = (TextView) findViewById(R.id.ename);
                Epin    = (TextView) findViewById(R.id.epin);
                Eyear   = (TextView) findViewById(R.id.eyear);
                Ebranch = (TextView) findViewById(R.id.ebranch);
                Esection= (TextView) findViewById(R.id.esection);
                Eemail  = (TextView) findViewById(R.id.eemail);
                Eblood  = (TextView) findViewById(R.id.eblood);
                Ephone  = (TextView) findViewById(R.id.ephone);
                Bgetdetails = (Button) findViewById(R.id.bgetdetails);
                Bsubmit	= (Button) findViewById(R.id.bsubmit);
                Onlinestatus = (TextView) findViewById(R.id.onlinestatus);

                //Bseteventname   = (Button) findViewById(R.id.bseteventname);
                //Bpassword       = (Button) findViewById(R.id.bpassword);
                Tpassword       = (TextView) findViewById(R.id.tpassword);
                Tnote = (TextView) findViewById(R.id.tnote);

                if(onlineflag==true){
                    try {
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        if (conMgr.getActiveNetworkInfo().isConnectedOrConnecting() && conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable()) {
                            Log.e("Network", "Connected");
                        }
                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                        Log.e("Network","Disconnected");
                        FragmentManager fm;
                        fm= getFragmentManager();
                        showDialog1 dialog =new showDialog1();
                        dialog.show(fm,"message");
                        dialog.setCancelable(false);
                    }
                    Onlinestatus.setText("•Online");
                    Onlinestatus.setTextColor(Color.parseColor("#ff00ff00"));

                }
                else{
                    Onlinestatus.setText("•Offline");
                    Onlinestatus.setTextColor(Color.parseColor("#ffff0000"));
                }



                /*File RCGUfolder = new File(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance");
                if (!RCGUfolder.exists()) {
                    boolean result = false;

                    try{
                        RCGUfolder.mkdir();
                        result = true;
                    }
                    catch(SecurityException se){
                        Toast.makeText(getApplicationContext(),"Cant create output Directory",Toast.LENGTH_SHORT).show();
                    }
                    if(result) {
                        Toast.makeText(getApplicationContext(),"RCGU Attendance DIR created in Internal Storage",Toast.LENGTH_SHORT).show();
                    }
                }*/
                File Images = new File(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/Images");
                if (!Images.exists()) {
                    boolean result = false;

                    try{
                        Images.mkdirs();
                        result = true;
                    }
                    catch(SecurityException se){
                        Toast.makeText(getApplicationContext(),"Cant create images output directory",Toast.LENGTH_SHORT).show();
                    }
                    if(result) {
                        Toast.makeText(getApplicationContext(),"DIR created in Internal Storage",Toast.LENGTH_SHORT).show();
                    }
                }


                // check if available and not read only
                if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
                    //  Log.w("FileUtils", "Storage not available or read only");
                    //return false;
                    Toast.makeText(getApplicationContext(),"External storage unavailable",Toast.LENGTH_SHORT).show();
                }





				//File dir = Environment.getExternalStorageDirectory();
				//File yourFile = new File(dir, "path/to/the/file/inside/the/sdcard.ext");
				//Get the text file
				File inputfile = new File(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/registered.json");
				// i have kept text.txt in the sd-card




                        if(inputfile.exists())   // check if file exist
                        {
                            //Read text from file


                            try {
                                BufferedReader br = new BufferedReader(new FileReader(inputfile));
                                String line;
                                while ((line = br.readLine()) != null) {
                                    text.append(line);
                                    text.append('\n');
                                }
					}
					catch (IOException e) {
						//You'll need to add proper error handling here
					}
					//Set the text
					//Toast.makeText(getApplicationContext(),text, Toast.LENGTH_SHORT).show();

				}
				else
				{
					//Toast.makeText(getApplicationContext(), "Sorry 'registered.json' file doesn't exist!!\nMake sure that the file is placed in RCGU DIR", Toast.LENGTH_SHORT).show();

					//tv.setText("Sorry file doesn't exist!!");
				}

                //text.append("{\"rcgu\": }");

				text1=text.toString();



				// we will using AsyncTask during parsing

                Bgetdetails.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View v) {

                        //Eid.requestFocus();
                        //Eid.setFocusableInTouchMode(true);

                        //InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        //imm.showSoftInput(Eid, InputMethodManager.SHOW_IMPLICIT);

                        InputMethodManager inputManager = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);

                        inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                        clearall();

                        id	=	Eid.getText().toString();
                        if(Tevent.getText().toString().length()==0){
                            Toast.makeText(getApplicationContext(),"Event name cant be empty\nAdd Event name by clicking on it",Toast.LENGTH_SHORT).show();
                        }
                        else if(id.length()==0){
                            Eid.setError("Enter ID or Phone Number");
                        }
                        else if(id.length()!=10){
                            setblankface();
                            Eid.setError("Please check your ID or Phone number");
                            //Toast.makeText(getApplicationContext(),"Enter the correct id or phone number",Toast.LENGTH_SHORT).show();
                        }
                        else if(onlineflag==true) {
                            connectionflag = false;

                            importSql a = new importSql();
                            a.execute();
                        }
                        else {
                            try{
                                connectionflag = true;
                                boolean flag=false;
                                FileInputStream input_document = new FileInputStream(new File(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/New Registrations"+".xls"));
                                HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document);
                                HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
                                Cell c = null;
                                int pos=0;
                                Row row1 = null;
                                try{
                                    for (int j=1; j< my_worksheet.getLastRowNum() + 1; j++) {
                                        row1 = my_worksheet.getRow(j);
                                        Cell cell1=null,cell7=null;
                                        if(row1.getCell(0).toString().length()!=0){cell1 = row1.getCell(3);}
                                        if(row1.getCell(0).toString().length()!=0){cell7 = row1.getCell(9);}
                                        if(new BigDecimal(cell1.toString()).toPlainString().equals(id)||new BigDecimal(cell7.toString()).toPlainString().equals(id)){
                                            flag=true;
                                            pos=j;
                                        }
                                    }
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if(flag==true){
                                    row1 = my_worksheet.getRow(pos);
                                    name = row1.getCell(0).toString()+" "+row1.getCell(1).toString()+" "+row1.getCell(2).toString();
                                    pin = new BigDecimal(row1.getCell(3).toString()).toPlainString();
                                    try{year= new BigDecimal(row1.getCell(4).toString()).toPlainString();}catch(Exception e){year = row1.getCell(4).toString();}
                                    branch = row1.getCell(5).toString();
                                    section = row1.getCell(6).toString();
                                    email = row1.getCell(7).toString();
                                    blood = row1.getCell(8).toString();
                                    phone = new BigDecimal(row1.getCell(9).toString()).toPlainString();
                                    hostel = row1.getCell(10).toString();
                                    local = row1.getCell(11).toString();
                                    college = row1.getCell(12).toString();
                                    dob = row1.getCell(13).toString();
                                    gender = row1.getCell(14).toString();
                                    term = row1.getCell(15).toString();
                                    hometown = row1.getCell(23).toString();
                                    bloodwilling = row1.getCell(24).toString();
                                    registered = row1.getCell(25).toString();
                                    postattendance(flag, connectionflag);
                                    if(registered.equals("0000-00-00 00:00:00")||registered.length()!=19){
                                        Tnote.setVisibility(View.VISIBLE);
                                    }
                                    else{
                                        Tnote.setVisibility(View.GONE);
                                    }
                                }
                                else{
                                    notregistered();
                                    //new AsyncTaskParseJson().execute();
                                }

                            }
                            catch (IOException e){
                                e.printStackTrace();
                                notregistered();
                                //new AsyncTaskParseJson().execute();
                            }
                        }



					}


				});

                Bsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(onlineflag==true){
                            try{
                                if(name.length()!=0) {

                                    exportSql a = new exportSql();
                                    a.execute();
                                }
                                else {
                                    Eid.setError("Enter ID");
                                }
                            }
                            catch (NullPointerException e){
                                if(Tevent.length()==0){
                                    Toast.makeText(getApplicationContext(),"Event name cant be empty\nAdd Event name by clicking on it",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    e.printStackTrace();
                                    Eid.setError("Enter ID or Phone Number");
                                }
                            }

                        }
                        else {
                            try{
                                if(name.length()!=0){
                                    if(new File(Environment.getExternalStorageDirectory().getPath()+"/RCGU Attendance/"+MainActivity.Tevent.getText().toString()+".xls").exists()){
                                        Log.e("Excel","is available");
                                        //Toast.makeText(getApplicationContext(),"Adding people to the Existing event",Toast.LENGTH_SHORT).show();
                                        exportExcel.addElement(getApplicationContext(),Tevent.getText().toString(),name,pin,year,branch,section,email,blood,phone,hostel,local,college,dob,gender,term,hometown,bloodwilling,registered);
                                    }
                                    else{
                                        Log.e("Excel","is not available");
                                        exportExcel.saveExcelFile(MainActivity.this);
                                        exportExcel.addElement(getApplicationContext(),Tevent.getText().toString(),name,pin,year,branch,section,email,blood,phone,hostel,local,college,dob,gender,term,hometown,bloodwilling,registered);
                                    }
                                }
                                else {
                                    Eid.setError("Enter ID");
                                }
                            }
                            catch (NullPointerException e){
                                if(Tevent.length()==0){
                                    Toast.makeText(getApplicationContext(),"Event name cant be empty\nAdd Event name by clicking on it",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    e.printStackTrace();
                                    Eid.setError("Enter ID or Phone Number");
                                }
                            }


                        /*Log.e("qwer","onclick");

                        String data="qwerty";
                        if(outputfile.exists())
                        {

                            try{
                                OutputStream fo = new FileOutputStream(outputfile);
                                fo.write(data.getBytes());
                                fo.close();

                                //url = upload.upload(file);
                            }
                            catch (FileNotFoundException e){
                                e.printStackTrace();
                            }
                            catch (IOException e){
                                e.printStackTrace();
                            }

                        }*/
                        }

                    }
                });
                Tevent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = new Dialog(MainActivity.this);
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.custom_dialog);
                        // Set dialog title
                        dialog.setTitle("Set Event Name");

                        // set values for custom dialog components - text, image and button

                        dialog.show();
                        Bpassword       = (Button)dialog.findViewById(R.id.bpassword);
                        Epassword       = (EditText) dialog.findViewById(R.id.epassword);
                        Eseteventname   = (EditText) dialog.findViewById(R.id.eseteventname);
                        Bpassword.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                            if (Epassword.getText().toString().equals("1234")) {
                                                Tevent.setText(Eseteventname.getText().toString());
                                                dialog.dismiss();
                                            } else {
                                                Epassword.setError("Invalid Password");
                                            }

                                    }
                                });


                    }
                });


            }
    public void setEvent(View v){


    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu){
        MenuItem offlinemenu = menu.findItem(R.id.offline_mode);
        MenuItem enablephotomenu = menu.findItem(R.id.enablephoto);

        if(onlineflag==true){
            offlinemenu.setTitle("Enable Offline mode");
        }
        if(onlineflag==false){
            offlinemenu.setTitle("Enable Online mode");
        }
        if(Enablephoto==true){
            enablephotomenu.setTitle("Disable Photo download");
        }
        if(Enablephoto==false){
            enablephotomenu.setTitle("Enable Photo download");
        }
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.settings:
                //startActivity(new Intent(this, com.slabs.rcguattendance.settings.class));
                return true;
            case R.id.about:
                //startActivity(new Intent(this, About.class));
                return true;
            case R.id.help:
                //startActivity(new Intent(this, Help.class));
                return true;*/
            case R.id.register:
                startActivity(new Intent(this, register.class));
                return true;
            case R.id.offline_mode:
                if(onlineflag==false){
                    onlineflag=true;
                    Onlinestatus.setText("•Online");
                    Onlinestatus.setTextColor(Color.parseColor("#ff00ff00"));

                    try {
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        if (conMgr.getActiveNetworkInfo().isConnectedOrConnecting() && conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable()) {
                            Log.e("Network", "Connected");

                        }
                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                        Log.e("Network","Disconnected");
                        FragmentManager fm;
                        fm= getFragmentManager();
                        showDialog1 dialog =new showDialog1();
                        dialog.show(fm,"message");
                        dialog.setCancelable(false);

                    }
                }
                else if(onlineflag==true){
                    onlineflag=false;
                    Onlinestatus.setText("•Offline");
                    Onlinestatus.setTextColor(Color.parseColor("#ffff0000"));

                }
                return true;
            case R.id.enablephoto:
                if(Enablephoto==true){
                    Enablephoto=false;
                }
                else if(Enablephoto==false){
                    Enablephoto=true;
                }
                return true;
            case R.id.uploadattendance:
                startActivity(new Intent(this, uploadImageDemo.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void setblankface() {
            Iface.setImageResource(R.mipmap.blankface);
    }

    private void clearall(){
        setblankface();
        Ename.setText(null);
        Epin.setText(null);
        Eyear.setText(null);
        Ebranch.setText(null);
        Esection.setText(null);
        Eemail.setText(null);
        Eblood.setText(null);
        Ephone.setText(null);
        name=pin=year=branch=section=email=blood=phone=null;
    }

    private void notregistered(){
        setblankface();
        FragmentManager fm;
        fm= getFragmentManager();
        showDialog dialog =new showDialog();
        dialog.show(fm,"message");
    }

    private  class  GetXMLTask  extends  AsyncTask<String,  Void,  Bitmap>  {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            setblankface();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Downloading image..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
		protected  Bitmap  doInBackground(String...  urls)  {
			Bitmap  map  =  null;
			for  (String  url  :  urls)  {

				map  =  getBitmapFromURL(url);
			}
			return  map;
		}
		//  Sets  the  Bitmap  returned  by  doInBackground
		@Override
		protected  void  onPostExecute(Bitmap  result)  {
            Iface.setImageDrawable(Drawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/Images/"+pin+".jpg"));
            pDialog.dismiss();
            //Toast.makeText(getApplicationContext(),String.valueOf(imagetotsize),Toast.LENGTH_SHORT).show();
		}

        public Bitmap getBitmapFromURL(String src) {
            try {
                URL url = new URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                imagetotsize=connection.getContentLength();
                InputStream input = connection.getInputStream();
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/Images/"+pin+".jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //publishProgress(""+(int)((total*100)/imagetotsize));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

                return null;
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

		//  Creates  Bitmap  from  InputStream  and  returns  it
		/*private  Bitmap  downloadImage(String  url)  {
			Bitmap  bitmap  =  null;
			InputStream  stream  =  null;
			BitmapFactory.Options  bmOptions  =  new  BitmapFactory.Options();
			bmOptions.inSampleSize  =  1;
			try  {
				stream  =  getHttpConnection(url);
				bitmap  =  BitmapFactory.decodeStream(stream,  null,  bmOptions);
				stream.close();
			}
			catch  (IOException  e1)  {
				e1.printStackTrace();
			}
			return  bitmap;
		}

		//  Makes  HttpURLConnection  and  returns  InputStream
		private  InputStream  getHttpConnection(String  urlString) throws  IOException  {
			InputStream  stream  =  null;
			URL  url  =  new  URL(URL);
			URLConnection  connection  =  url.openConnection();
			try  {
				HttpURLConnection  httpConnection  =  (HttpURLConnection)  connection;
				httpConnection.setRequestMethod("GET");
				httpConnection.connect();

				if  (httpConnection.getResponseCode()  ==  HttpURLConnection.HTTP_OK)  {
					stream  =  httpConnection.getInputStream();
				}
			}
			catch  (Exception  ex)  {
				ex.printStackTrace();
			}
			return  stream;
		}*/
	}
	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {




        boolean flag;

        String text1= text.toString();
		final String TAG = "AsyncTaskParseJson.java";

		// set your json string url here
		//String yourJsonStringUrl = "http://demo.codeofaninja.com/tutorials/json-example-with-php/index.php";

		// contacts JSONArray
		JSONArray dataJsonArr = null;

		@Override
		protected void onPreExecute() {}

		@Override
		protected String doInBackground(String... arg0) {

			try {

				// get json string from url
				JSONObject json = new JSONObject(text1);

				// get the array of users
				dataJsonArr = json.getJSONArray("rcgu");

				// loop through all users
				for (int i = 0; i < dataJsonArr.length(); i++) {

					JSONObject c = dataJsonArr.getJSONObject(i);

                    flag=false;
					// Storing each json item in variable
					name = c.getString("Name");
					pin = c.getString("Pin");
					year= c.getString("Year");
                    branch = c.getString("Branch");
                    section = c.getString("Section");
                    email = c.getString("Email");
                    blood = c.getString("Blood Group");
                    phone = c.getString("Phone");
                    hostel = c.getString("Hostel");
                    local = c.getString("Local");
                    college = c.getString("College");
                    dob = c.getString("Date of Birth");
                    gender = c.getString("Gender");
                    term = c.getString("Term");
                    hometown = c.getString("Home Town");
                    bloodwilling = c.getString("Blood Donation Willingness");
                    registered = c.getString("Registered on");

                    if (id.equals(phone) || id.equals(pin)) {
                        flag=true;
                        break;
                    }
                    else if (flag == false) {
                        name=pin=year=branch=section=email=blood=phone=hostel=local=college=dob=gender=term=hometown=bloodwilling=registered="";

                    }
				}

			} catch (JSONException e) {
				e.printStackTrace();
            }

			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
            postattendance(flag,connectionflag);
            if(registered.equals("0000-00-00 00:00:00")||registered.length()!=19){
                Tnote.setVisibility(View.VISIBLE);
            }
            else{
                Tnote.setVisibility(View.GONE);
            }
        }
    }

    public void postattendance(boolean flag, boolean connection){

        final String TAG = "AsyncTaskParseJson.java";

        //set(name, pin, year, branch, section, email, blood, phone);
        Ename.setText(name);
        Epin.setText(pin);
        Eyear.setText(year);
        Ebranch.setText(branch);
        Esection.setText(section);
        Eemail.setText(email);
        Eblood.setText(blood);
        Ephone.setText(phone);
        Log.e(TAG, "firstname: " + name
                + ", lastname: " + pin
                + ", username: " + phone +"flag: "+ flag);





        File file = new File(Environment.getExternalStorageDirectory().getPath() +"/RCGU Attendance/Images/"+pin+".jpg");
        ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        try{
            if(flag==false) {
                if(connection == true){
                    notregistered();
                }
                if(connection == false){
                    Toast.makeText(getApplicationContext(),"Connection failed, Try again..",Toast.LENGTH_SHORT).show();
                }


            }
            //else if ( conMgr.getActiveNetworkInfo().isConnectedOrConnecting() && conMgr.getActiveNetworkInfo()!=null && conMgr.getActiveNetworkInfo().isAvailable() ) {
            //Log.e("network ", "connected");

            else if(pincheck.equals(pin)||pin.equals("")){

            }

            else if(file.exists()){

                Iface.setImageDrawable(Drawable.createFromPath(file.toString()));
            }

            else if(Enablephoto==false){
                setblankface();
            }

            else if(conMgr.getActiveNetworkInfo().isConnectedOrConnecting() && conMgr.getActiveNetworkInfo()!=null && conMgr.getActiveNetworkInfo().isAvailable()) {
                Log.e("network ", "connected");


                //  Create  an  object  for  subclass  of  AsyncTask

                Log.e("file not available", "entered");
                GetXMLTask task = new GetXMLTask();
                //  Execute  the  task

                URL = "https://doeresults.gitam.edu/gitamhallticket/img.aspx?id=";
                URL = URL + pin;
                task.execute(URL);
                pincheck = pin;
            }

            else  {
                //else is not working
                // notify user you are not online
            }


        }
        catch (NullPointerException e){
            Log.e("network ", "disconnected");
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Network connection is required to load photo",Toast.LENGTH_SHORT).show();

        }




        // show the values in our logcat}
    }

    class  importSql extends AsyncTask<String, String, String> {
        String w;
        String result = "";
        boolean flag;
        private ProgressDialog pDialog;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Fetching Details..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("id",id));
            try
            {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://rcgu.org/attendanceapp/select.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connectionflag = true;

                Log.e("log_tag", "connection success ");
                //   Toast.makeText(getApplicationContext(), "pass", Toast.LENGTH_SHORT).show();
            }
            catch(Exception e)
            {
                e.printStackTrace();
                Log.e("log_tag", "Error in http connection "+e.toString());
                //Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

            }
            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                    //  Toast.makeText(getApplicationContext(), "Input Reading pass", Toast.LENGTH_SHORT).show();
                }
                is.close();

                result=sb.toString();
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error converting result "+e.toString());
                //Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();

            }

            //parse json data
            try{


                JSONObject object = new JSONObject(result);
                String ch=object.getString("re");
                if(ch.equals("success"))
                {

                    JSONObject no = object.getJSONObject("0");

                    //long q=object.getLong("f1");
                    name = no.getString("Name");
                    pin = no.getString("Pin");
                    year= no.getString("Year");
                    branch = no.getString("Branch");
                    section = no.getString("Section");
                    email = no.getString("Email");
                    blood = no.getString("Blood Group");
                    phone = no.getString("Phone");
                    hostel = no.getString("Hostel");
                    local = no.getString("Local");
                    college = no.getString("College");
                    dob = no.getString("Date of Birth");
                    gender = no.getString("Gender");
                    term = no.getString("Term");
                    hometown = no.getString("Home Town");
                    bloodwilling = no.getString("Blood Donation Willingness");
                    registered = no.getString("Registered on");
                    flag=true;


                    //long e=no.getLong("Phone");

                    //Ename.setText(w);
                    //String myString = NumberFormat.getInstance().format(e);


                    //MainActivity.Ename.setText(myString);

                }


                else
                {

                    //Toast.makeText(getApplicationContext(), "Record is not available.. Enter valid number", Toast.LENGTH_SHORT).show();
                    flag=false;

                }


            }
            catch(JSONException e)
            {
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
                //Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
            }
            return  "";
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            if(result.contains("Authentication is required to access the requested web site (&nbsp;wsa2.gitam.edu&nbsp;).")){
                Toast.makeText(getApplicationContext(),"You are using GITAM internet\nPlease use any VPN to go on.",Toast.LENGTH_SHORT).show();
            }
            else{
                if(flag==true){
                    if(registered.equals("0000-00-00 00:00:00")||registered.length()!=19){
                        Tnote.setVisibility(View.VISIBLE);
                    }
                    else{
                        Tnote.setVisibility(View.GONE);
                    }
                }
                postattendance(flag, connectionflag);
            }

        }

    }

    class  exportSql extends AsyncTask<String, String, String> {
        CharSequence w="";
        String event = Tevent.getText().toString();
        String result = "";
        boolean connection=false;
        private ProgressDialog pDialog;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Processing..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            //String v1 = editText.getText().toString();
            //String v2 = editText1.getText().toString();
            //String v3 = editText2.getText().toString();
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("event",event));
            nameValuePairs.add(new BasicNameValuePair("name",name));
            nameValuePairs.add(new BasicNameValuePair("pin",pin));
            nameValuePairs.add(new BasicNameValuePair("year",year));
            nameValuePairs.add(new BasicNameValuePair("branch",branch));
            nameValuePairs.add(new BasicNameValuePair("section",section));
            nameValuePairs.add(new BasicNameValuePair("email",email));
            nameValuePairs.add(new BasicNameValuePair("blood",blood));
            nameValuePairs.add(new BasicNameValuePair("phone",phone));
            nameValuePairs.add(new BasicNameValuePair("hostel",hostel));
            nameValuePairs.add(new BasicNameValuePair("local",local));
            nameValuePairs.add(new BasicNameValuePair("college",college));
            nameValuePairs.add(new BasicNameValuePair("dob",dob));
            nameValuePairs.add(new BasicNameValuePair("gender",gender));
            nameValuePairs.add(new BasicNameValuePair("term",term));
            nameValuePairs.add(new BasicNameValuePair("hometown",hometown));
            nameValuePairs.add(new BasicNameValuePair("bloodwilling",bloodwilling));
            nameValuePairs.add(new BasicNameValuePair("registered",registered));



            StrictMode.setThreadPolicy(policy);


            //http post
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://rcgu.org/attendanceapp/insert.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;

                Log.e("log_tag", "connection success ");
                //Toast.makeText(context, "pass", Toast.LENGTH_SHORT).show();
            }


            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
                //Toast.makeText(context, "Connection fail", Toast.LENGTH_SHORT).show();

            }
            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                    //Intent i = new Intent(getBaseContext(),DatabaseActivity.class);
                    //startActivity(i);
                }
                is.close();

                result=sb.toString();
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error converting result "+e.toString());
            }


            try{

                JSONObject json_data = new JSONObject(result);

                 w= (CharSequence) json_data.get("re");

                //Toast.makeText(context, w, Toast.LENGTH_SHORT).show();


            }
            catch(JSONException e)
            {
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
                //Toast.makeText(context, "JsonArray fail", Toast.LENGTH_SHORT).show();
            }

            return  "";

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            if(w.length()!=0) {
                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
            }
            if(connection == false){
                Toast.makeText(getApplicationContext(),"Connection failed, Try again..", Toast.LENGTH_SHORT).show();
            }
        }

    }



    public class showDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You are not registered")
                    .setPositiveButton("Register Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!
                            startActivity(new Intent(MainActivity.this, com.slabs.rcguattendance.register.class));

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public class showDialog1 extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You are not connected to internet")
                    .setPositiveButton("Enable Offline mode", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            onlineflag=false;
                            Onlinestatus.setText("•Offline");
                            Onlinestatus.setTextColor(Color.parseColor("#ffff0000"));
                            try{
                                register.Regonlinestatus.setText("•Offline");
                                register.Regonlinestatus.setTextColor(Color.parseColor("#ffff0000"));
                            }
                            catch (NullPointerException e){
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            finish();
                            System.exit(0);
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }


    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }


}

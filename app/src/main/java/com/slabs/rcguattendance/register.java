package com.slabs.rcguattendance;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.InputType;
import android.text.format.Formatter;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.openxml4j.opc.internal.marshallers.PackagePropertiesMarshaller;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Sreekanth Putta on 14-05-2016.
 */
public class register extends Activity implements AdapterView.OnItemSelectedListener {
    public static TextView Regonlinestatus;
    EditText Regname, Regpin, Regemail, Regphone, Regdob, Reghometown, Regsection, Regprogram, Regblood, Regmiddlename, Reglastname, Regaddress1, Regaddress2, Regaddress3, Regcity, Regpostal, Regstate;
    RadioButton Regmale, Regfemale, Reghostelyes, Reghostelno, Reglocalyes, Reglocalno, Regbloodyes, Regbloodno;
    RadioGroup Reggenderselect, Reghostelselect, Reglocalselect, Regbloodselect, Regpermanentselect;
    Spinner Regcollege, Regbranch, Regyear, Regterm;
    Button Regsubmit, Regreset;
    int colspinner, yearspinner;
    String name, middlename, lastname, pin, year, branch, section, email, blood, phone, hostel, local, address1, address2, address3, city, postal, state, permanent, college, dob, gender, term, hometown, bloodwilling, registeredondevice, registeredondeviceinternalip, registeredondeviceexternalip, wifimacaddress, wifibssid, wifiipaddress, bluetoothmacaddress;

    String[] scollege = new String[] {"GIT", "GIS", "GIM", "GSIB", "GIP", "GSA", "GSL", "CDL"};
    String[] sbranchgit = new String[] {"Biotechnology", "Civil", "CSE", "EEE", "ECE", "EIE", "IE", "IT", "Mechanical", "Physics", "Chemistry","English", "Maths"};
    String[] sbranchgis = new String[] {"Applied Mathematics", "Biochemistry", "Biotechnology", "Chemistry", "Computer Science", "Environmental Studies", "Electronics/Physics", "Microbiology", "English"};
    String[] syear = new String[] {"1", "2", "3", "4", "5"};
    String[] sterm1 = new String[] {"2016-20", "2016-21"};
    String[] sterm2 = new String[] {"2015-19", "2015-20"};
    String[] sterm3 = new String[] {"2014-18", "2014-19"};
    String[] sterm4 = new String[] {"2013-17", "2013-18"};
    String[] sterm5 = new String[] {"2012-16", "2012-17"};
    String[] snothing = new String[] {""};
    ArrayAdapter<String> adapter;

    private DatePickerDialog dobPickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        Regname = (EditText) findViewById(R.id.regname);
        Regpin = (EditText) findViewById(R.id.regpin);
        Regemail = (EditText) findViewById(R.id.regemail);
        Regphone = (EditText) findViewById(R.id.regphone);
        //Regmale = (RadioButton) findViewById(R.id.regmale);
        //Regfemale = (RadioButton) findViewById(R.id.regfemale);
        Regdob = (EditText) findViewById(R.id.regdob);
        //Reghostelyes = (RadioButton) findViewById(R.id.reghostelyes);
        //Reghostelno = (RadioButton) findViewById(R.id.reghostelno);
        //Reglocalyes = (RadioButton) findViewById(R.id.reglocalyes);
        //Reglocalno = (RadioButton) findViewById(R.id.reglocalno);
        Reghometown = (EditText) findViewById(R.id.reghometown);
        Regcollege = (Spinner) findViewById(R.id.regcollege);
        Regbranch = (Spinner) findViewById(R.id.regbranch);
        Regprogram = (EditText) findViewById(R.id.regprogram);
        Regyear = (Spinner) findViewById(R.id.regyear);
        Regterm = (Spinner) findViewById(R.id.regterm);
        Regsection = (EditText) findViewById(R.id.regsection);
        Regblood = (EditText) findViewById(R.id.regblood);
        //Regbloodyes = (RadioButton) findViewById(R.id.regbloodyes);
        //Regbloodno = (RadioButton) findViewById(R.id.regbloodno);
        Regsubmit = (Button) findViewById(R.id.regsubmit);
        Regreset = (Button) findViewById(R.id.regreset);
        Reggenderselect = (RadioGroup) findViewById(R.id.reggenderselect);
        Reghostelselect = (RadioGroup) findViewById(R.id.reghostelselect);
        Reglocalselect = (RadioGroup) findViewById(R.id.reglocalselect);
        Regbloodselect = (RadioGroup) findViewById(R.id.regbloodselect);
        Regonlinestatus = (TextView) findViewById(R.id.regonlinestatus);
        Regmiddlename = (EditText) findViewById(R.id.regmiddlename);
        Reglastname = (EditText) findViewById(R.id.reglastname);
        Regaddress1 = (EditText) findViewById(R.id.regaddress1);
        Regaddress2 = (EditText) findViewById(R.id.regaddress2);
        Regaddress3 = (EditText) findViewById(R.id.regaddress3);
        Regcity = (EditText) findViewById(R.id.regcity);
        Regpostal = (EditText) findViewById(R.id.regpostal);
        Regstate = (EditText) findViewById(R.id.regstate);
        Regpermanentselect = (RadioGroup) findViewById(R.id.regpermanentselect);

        if (MainActivity.onlineflag == true) {
            Regonlinestatus.setText("•Online");
            Regonlinestatus.setTextColor(Color.parseColor("#ff00ff00"));
        }
        if (MainActivity.onlineflag == false) {
            Regonlinestatus.setText("•Offline");
            Regonlinestatus.setTextColor(Color.parseColor("#ffff0000"));
        }

        Regname.requestFocus();
        Regdob.setInputType(InputType.TYPE_NULL);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, scollege);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Regcollege.setPrompt("Select your favorite Planet!");
        Regcollege.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.college_spinner_row_nothing_selected, this));
        Regcollege.setOnItemSelectedListener(this);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, snothing);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Regbranch.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.branch_spinner_row_nothing_selected, this));

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, syear);
        Regyear.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.year_spinner_row_nothing_selected, this));
        Regyear.setOnItemSelectedListener(this);


        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, snothing);
        Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));


        Regdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar newCalendar = Calendar.getInstance();

                dobPickerDialog = new DatePickerDialog(register.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Regdob.setText(dateFormatter.format(newDate.getTime()));
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


                if (v == Regdob) {
                    dobPickerDialog.show();
                }
            }
        });

        Regsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String radio;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    radio=Build.getRadioVersion();
                }
                else{
                    radio=Build.RADIO;
                }
                registeredondevice=registeredondeviceinternalip=null;
                try {
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress()) {
                                registeredondeviceinternalip = "; Internal IP:"+inetAddress.getHostAddress().toString();
                            }
                        }
                    }
                } catch (Exception ex) {
                    Log.e("IP Address", ex.toString());
                }
                try{
                    new getexternalip().execute().get();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

                String ssbranch, sslocal, ssblood, sshostel, ssgender, sspermanent;
                ssbranch = sslocal = ssblood = sshostel = ssgender = sspermanent = "";
                boolean branchflag = false;
                if (Regbranch.getVisibility() == View.VISIBLE) {
                    if (Regbranch.getSelectedItemPosition() > 1) {
                        ssbranch = Regbranch.getSelectedItem().toString();
                        branchflag = true;
                    }
                } else {
                    if (Regprogram.getText().length() != 0) {
                        ssbranch = Regprogram.getText().toString();
                        branchflag = true;
                    }
                }
                /*
                if (Reggenderselect.getCheckedRadioButtonId() == 1) {
                    gender = "Male";
                } else if (Regbloodselect.getCheckedRadioButtonId() == 2) {
                    gender = "Female";
                }
                if (Reghostelselect.getCheckedRadioButtonId() == 3) {
                    hostel = "Yes";
                } else if (Reghostelselect.getCheckedRadioButtonId() == 4) {
                    hostel = "No";
                }
                if (Reglocalselect.getCheckedRadioButtonId() == 5) {
                    local = "Yes";
                } else if (Reglocalselect.getCheckedRadioButtonId() == 6) {
                    local = "No";
                }
                if (Regbloodselect.getCheckedRadioButtonId() == 2131427392) {
                    blood = "Yes";
                } else if (Regbloodselect.getCheckedRadioButtonId() == 2131427393) {
                    blood = "No";
                }*/

                Time time = new Time();
                time.setToNow();


                //int i=Regbloodselect.getCheckedRadioButtonId();
                Log.e("radio", String.valueOf(Reggenderselect.getCheckedRadioButtonId()) + " " + String.valueOf(Reghostelselect.getCheckedRadioButtonId()) + " " + String.valueOf(Reglocalselect.getCheckedRadioButtonId()) + " " + String.valueOf(Regbloodselect.getCheckedRadioButtonId()));

                if (
                        Regname.getText().length() == 0
                                //        || Regmiddlename.getText().length() ==0
                                || Reglastname.getText().length() ==0
                                || Regpin.getText().length() == 0
                                || Regyear.getSelectedItemPosition() < 1
                                || !branchflag
                                //        || Regsection.getText().length() == 0
                                || Regemail.getText().length() == 0
                                //        || Regblood.getText().length() == 0
                                || Regphone.getText().length() == 0
                                || Regdob.getText().length() == 0
                                || Reghostelselect.getCheckedRadioButtonId() < 1
                                || Reglocalselect.getCheckedRadioButtonId() < 1
                                || Regaddress1.getText().length()==0
                                //        || Regaddress2.getText().length()==0
                                //        || Regaddress3.getText().length()==0
                                || Regcity.getText().length()==0
                                || Regpostal.getText().length()==0
                                || Regstate.getText().length()==0
                                || Regpermanentselect.getCheckedRadioButtonId() < 1
                                || Regcollege.getSelectedItemPosition() < 1
                                || Regterm.getSelectedItemPosition() < 1
                                || Reghometown.getText().length() == 0
                    //        || Regbloodselect.getCheckedRadioButtonId() < 1
                        ) {
                    Toast.makeText(getApplicationContext(), "Required field is empty", Toast.LENGTH_SHORT).show();
                    Log.e("Fields",Regmiddlename.getText().length()+"   "+ Reglastname.getText().length()+"   "+Regpin.getText().length()+"   "+Regyear.getSelectedItemPosition()+"   "+!branchflag+"   "+Regsection.getText().length()+"   "+Regemail.getText().length()+"   "+
                            Regblood.getText().length()+"   "+Regphone.getText().length()+"   "+Regdob.getText().length()+"   "+Reghostelselect.getCheckedRadioButtonId()+"   "+Reglocalselect.getCheckedRadioButtonId()+"   "+Regaddress1.getText().length()+"   "+
                            Regaddress2.getText().length()+"   "+Regaddress3.getText().length()+"   "+Regcity.getText().length()+"   "+Regpostal.getText().length()+"   "+Regstate.getText().length()+"   "+Regpermanentselect.getCheckedRadioButtonId()+"   "+
                            Regcollege.getSelectedItemPosition()+"   "+Regterm.getSelectedItemPosition()+"   "+Reghometown.getText().length()+"   "+Regbloodselect.getCheckedRadioButtonId());
                } else if (Long.valueOf(Regpin.getText().toString())>2147483647 || Regpin.getText().length() < 9 || Regphone.getText().length() < 10) {
                    Toast.makeText(getApplicationContext(), "Incorrect College ID or Phone Number", Toast.LENGTH_SHORT).show();
                } else {
                    int radioid;
                    RadioButton radioButton;

                    radioid = Reggenderselect.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radioid);
                    ssgender = radioButton.getText().toString();

                    radioid = Reghostelselect.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radioid);
                    sshostel = radioButton.getText().toString();

                    radioid = Reglocalselect.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radioid);
                    sslocal = radioButton.getText().toString();

                    radioid = Regbloodselect.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radioid);
                    ssblood = radioButton.getText().toString();

                    radioid = Regpermanentselect.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radioid);
                    sspermanent = radioButton.getText().toString();

                    if (MainActivity.onlineflag == true) {
                        name = Regname.getText().toString();
                        middlename = Regmiddlename.getText().toString();
                        lastname = Reglastname.getText().toString();
                        pin = Regpin.getText().toString();
                        year = Regyear.getSelectedItem().toString();
                        branch = ssbranch;
                        section = Regsection.getText().toString();
                        email = Regemail.getText().toString();
                        blood = Regblood.getText().toString();
                        phone = Regphone.getText().toString();
                        hostel = sshostel;
                        local = sslocal;
                        address1 = Regaddress1.getText().toString();
                        address2 = Regaddress2.getText().toString();
                        address3 = Regaddress3.getText().toString();
                        city = Regcity.getText().toString();
                        postal = Regpostal.getText().toString();
                        state = Regstate.getText().toString();
                        permanent = sspermanent;
                        hometown = Reghometown.getText().toString();
                        college = Regcollege.getSelectedItem().toString();
                        dob = Regdob.getText().toString();
                        gender = ssgender;
                        term = Regterm.getSelectedItem().toString();
                        bloodwilling = ssblood;
                        registeredondevice="Interface:Android App"+registeredondeviceinternalip+registeredondeviceexternalip+"; Device:"+Build.DEVICE+"; Model:"+ Build.MODEL+"; Brand:"+Build.BRAND+"; Product:"+Build.PRODUCT+"; Manufacturer:"+Build.MANUFACTURER+"; User:"+Build.USER+"; Serial:"+Build.SERIAL+"; Connected to Wifi:"+getWifiName(getApplicationContext())+"; Wifi IP Address:"+wifiipaddress+"; Wifi SSID:"+wifimacaddress+"; Wifi BSSID:"+wifibssid+"; Bluetooth Name:"+getLocalBluetoothName()+"; Bluetooth Mac Address:"+bluetoothmacaddress+"; API Version:"+Build.VERSION.SDK_INT+"; Supported ABIS:"+Build.CPU_ABI+"; Board:"+Build.BOARD+"; Bootloader:"+Build.BOOTLOADER+"; Hardware:"+Build.HARDWARE+"; Host:"+Build.HOST+"; ID:"+Build.ID+"; Radio:"+radio+"; Type:"+Build.TYPE;
                        regexportSql a = new regexportSql();
                        a.execute();
                        //Toast.makeText(getApplicationContext(),registeredondevice,Toast.LENGTH_SHORT).show();
                    } else {
                        if (!(new File(Environment.getExternalStorageDirectory().getPath() + "/RCGU Attendance/New Registrations" + ".xls").exists())) {

                            exportExcel.createReg(getApplicationContext());
                        }
                        exportExcel.addElementReg(getApplicationContext(),
                                MainActivity.Tevent.getText().toString(),
                                Regname.getText().toString(),
                                Regmiddlename.getText().toString(),
                                Reglastname.getText().toString(),
                                Regpin.getText().toString(),
                                Regyear.getSelectedItem().toString(),
                                ssbranch,
                                Regsection.getText().toString(),
                                Regemail.getText().toString(),
                                Regblood.getText().toString(),
                                Regphone.getText().toString(),
                                sshostel,
                                sslocal,
                                Regaddress1.getText().toString(),
                                Regaddress2.getText().toString(),
                                Regaddress3.getText().toString(),
                                Regcity.getText().toString(),
                                Regpostal.getText().toString(),
                                Regstate.getText().toString(),
                                sspermanent,
                                Reghometown.getText().toString(),
                                Regcollege.getSelectedItem().toString(),
                                Regdob.getText().toString(),
                                ssgender,
                                Regterm.getSelectedItem().toString(),
                                ssblood,
                                time.toString(),
                                registeredondevice);
                    }
                    //Regreset.performClick();
                }
            }
        });

        Regreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regname.getText().clear();
                Regmiddlename.getText().clear();
                Reglastname.getText().clear();
                Regpin.getText().clear();
                Regemail.getText().clear();
                Regphone.getText().clear();
                Reggenderselect.clearCheck();
                Regdob.getText().clear();
                Reghostelselect.clearCheck();
                Reglocalselect.clearCheck();
                Regaddress1.getText().clear();
                Regaddress2.getText().clear();
                Regaddress3.getText().clear();
                Regcity.getText().clear();
                Regpostal.getText().clear();
                Regstate.getText().clear();
                Regpermanentselect.clearCheck();
                Reghometown.getText().clear();
                Regcollege.setSelection(0);
                Regbranch.setSelection(0);
                Regyear.setSelection(0);
                Regterm.setSelection(0);
                Regsection.getText().clear();
                Regblood.getText().clear();
                Regbloodselect.clearCheck();
            }
        });

    }
    @Override
     public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {

        int colid = Regcollege.getSelectedItemPosition();
        int yearid = Regyear.getSelectedItemPosition();
        if (colid!=colspinner) {
            if (colid == 1) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sbranchgit);
                Regbranch.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.branch_spinner_row_nothing_selected, this));
                Regbranch.setVisibility(View.VISIBLE);
                Regprogram.setVisibility(View.GONE);
            }
            if (colid == 2) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sbranchgis);
                Regbranch.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.branch_spinner_row_nothing_selected, this));
                Regbranch.setVisibility(View.VISIBLE);
                Regprogram.setVisibility(View.GONE);
            }
            if (colid == 3 || colid == 4 || colid == 5 || colid == 6 || colid == 7 || colid == 8) {
                Regbranch.setVisibility(View.GONE);
                Regprogram.setVisibility(View.VISIBLE);
            }
            colspinner = colid;
        }
        if (yearid!=yearspinner) {
            if (yearid == 1) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sterm1);
                Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));
            }
            if (yearid == 2) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sterm2);
                Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));
            }
            if (yearid == 3) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sterm3);
                Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));
            }
            if (yearid == 4) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sterm4);
                Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));
            }
            if (yearid == 5) {
                adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sterm5);
                Regterm.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.term_spinner_row_nothing_selected, this));
            }
            yearspinner = yearid;
        }




        Log.e(String.valueOf(arg0), String.valueOf(arg1));

        // TODO Auto-generated method stub
        /*String sp1= String.valueOf(s1.getSelectedItem());
        Toast.makeText(this, sp1, Toast.LENGTH_SHORT).show();
        if(sp1.contentEquals("Income")) {
            List<String> list = new ArrayList<String>();
            list.add("Salary");
            list.add("Sales");
            list.add("Others");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter.notifyDataSetChanged();
            s2.setAdapter(dataAdapter);
        }
        if(sp1.contentEquals("Expense")) {
            List<String> list = new ArrayList<String>();
            list.add("Conveyance");
            list.add("Breakfast");
            list.add("Purchase");
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter2.notifyDataSetChanged();
            s2.setAdapter(dataAdapter2);
        }*/

    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.registermenu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu){
        MenuItem offlinemenu = menu.findItem(R.id.regoffline_mode);

        if(MainActivity.onlineflag==true){
            offlinemenu.setTitle("Enable Offline mode");
        }
        if(MainActivity.onlineflag==false){
            offlinemenu.setTitle("Enable Online mode");
        }
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.regoffline_mode:
                if(MainActivity.onlineflag==false){
                    MainActivity.onlineflag=true;
                    Regonlinestatus.setText("•Online");
                    Regonlinestatus.setTextColor(Color.parseColor("#ff00ff00"));

                    try {
                        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        if (conMgr.getActiveNetworkInfo().isConnectedOrConnecting() && conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable()) {
                            Log.e("Network", "Connected");
                        }
                        if("message".toString().contains("Registered Successfully")){
                            Regreset.performClick();
                        }
                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                        Log.e("Network","Disconnected");
                        FragmentManager fm;
                        fm= getFragmentManager();
                        MainActivity mainActivity = new MainActivity();
                        MainActivity.showDialog1 dialog =mainActivity.new showDialog1();
                        dialog.show(fm,"message");
                        dialog.setCancelable(false);

                    }
                }
                else if(MainActivity.onlineflag==true){
                    MainActivity.onlineflag=false;
                    Regonlinestatus.setText("•Offline");
                    Regonlinestatus.setTextColor(Color.parseColor("#ffff0000"));

                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class regexportSql extends AsyncTask<String, String, String> {
        CharSequence w="";
        String result = "";
        boolean connection=false;
        private ProgressDialog pDialog;

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(register.this);
            pDialog.setMessage("Processing..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            //String v1 = editText.getText().toString();
            //String v2 = editText1.getText().toString();
            //String v3 = editText2.getText().toString();
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("name",name));
            nameValuePairs.add(new BasicNameValuePair("middlename",middlename));
            nameValuePairs.add(new BasicNameValuePair("lastname",lastname));
            nameValuePairs.add(new BasicNameValuePair("pin",pin));
            nameValuePairs.add(new BasicNameValuePair("year",year));
            nameValuePairs.add(new BasicNameValuePair("branch",branch));
            nameValuePairs.add(new BasicNameValuePair("section",section));
            nameValuePairs.add(new BasicNameValuePair("email",email));
            nameValuePairs.add(new BasicNameValuePair("blood",blood));
            nameValuePairs.add(new BasicNameValuePair("phone",phone));
            nameValuePairs.add(new BasicNameValuePair("hostel",hostel));
            nameValuePairs.add(new BasicNameValuePair("local",local));
            nameValuePairs.add(new BasicNameValuePair("address1",address1));
            nameValuePairs.add(new BasicNameValuePair("address2",address2));
            nameValuePairs.add(new BasicNameValuePair("address3",address3));
            nameValuePairs.add(new BasicNameValuePair("city",city));
            nameValuePairs.add(new BasicNameValuePair("postal",postal));
            nameValuePairs.add(new BasicNameValuePair("state",state));
            nameValuePairs.add(new BasicNameValuePair("permanent",permanent));
            nameValuePairs.add(new BasicNameValuePair("hometown",hometown));
            nameValuePairs.add(new BasicNameValuePair("college",college));
            nameValuePairs.add(new BasicNameValuePair("dob",dob));
            nameValuePairs.add(new BasicNameValuePair("gender",gender));
            nameValuePairs.add(new BasicNameValuePair("term",term));
            nameValuePairs.add(new BasicNameValuePair("bloodwilling",bloodwilling));
            nameValuePairs.add(new BasicNameValuePair("registeredondevice",registeredondevice));


            StrictMode.setThreadPolicy(policy);


            //http post
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://rcgu.org/attendanceapp/reginsert.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;

                Log.e("log_tag", "connection success ");
                //Toast.makeText(context, "pass", Toast.LENGTH_SHORT).show();
            }


            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
                //Toast.makeText(context, "Connection fail", Toast.LENGTH_SHORT).show();

            }
            //convert response to string
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                    //Intent i = new Intent(getBaseContext(),DatabaseActivity.class);
                    //startActivity(i);
                }
                is.close();

                result=sb.toString();
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error converting result "+e.toString());
            }


            try{

                JSONObject json_data = new JSONObject(result);

                w= (CharSequence) json_data.get("re");

                //Toast.makeText(context, w, Toast.LENGTH_SHORT).show();


            }
            catch(JSONException e)
            {
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
                //Toast.makeText(context, "JsonArray fail", Toast.LENGTH_SHORT).show();
            }

            return  "";

        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            if(result.contains("Authentication is required to access the requested web site (&nbsp;wsa2.gitam.edu&nbsp;).")){
                Toast.makeText(getApplicationContext(),"You are using GITAM internet\nPlease use any VPN to go on.",Toast.LENGTH_SHORT).show();
            }
            else{
                if(w.length()!=0) {
                    Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
                }
                if(connection == false){
                    Toast.makeText(getApplicationContext(),"Connection failed, Try again..", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    class getexternalip extends AsyncTask<String, String, String>{
        String externalIP=null, error=null;
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(register.this);
            pDialog.setMessage("Checking IP address..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            registeredondeviceexternalip=null;
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet("http://whatismyip.akamai.com/");
                // HttpGet httpget = new HttpGet("http://ipecho.net/plain");
                HttpResponse response;

                response = httpclient.execute(httpget);

                //Log.i("externalip",response.getStatusLine().toString());

                HttpEntity entity = response.getEntity();
                externalIP = EntityUtils.toString(entity);
                if (externalIP != null) {
                    long len = externalIP.length();
                    if (len != -1 && len < 1024) {
                        String str= externalIP;
                        //Log.i("externalip",str);
                        registeredondeviceexternalip="; External IP:"+str;
                    } else {
                        error = "Response too long or error.";
                        //debug
                        //ip.setText("Response too long or error: "+EntityUtils.toString(entity));
                        //Log.i("externalip",EntityUtils.toString(entity));
                    }
                } else {
                    error="Null:"+response.getStatusLine().toString();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                error="Error checking IP address.";
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if(externalIP.contains("Authentication is required to access the requested web site (&nbsp;wsa2.gitam.edu&nbsp;).")){
                }
            else if(externalIP==null){
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
            }
        }
    }
    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = manager.getConnectionInfo();
        Log.e("Wifi",wifiInfo.toString());
        wifimacaddress = wifiInfo.getMacAddress();
        wifibssid = wifiInfo.getBSSID();
        int ipAddress = wifiInfo.getIpAddress();
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get host address.");
            ipAddressString = null;
        }
        wifiipaddress= ipAddressString;
        return wifiInfo.getSSID();
    }
    public String getLocalBluetoothName(){
    BluetoothAdapter mBluetoothAdapter=null;
        if(mBluetoothAdapter == null){
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String name = mBluetoothAdapter.getName();
        bluetoothmacaddress = mBluetoothAdapter.getAddress();
        return name;
    }


}

